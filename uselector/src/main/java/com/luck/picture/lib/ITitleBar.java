package com.luck.picture.lib;

import android.view.View;

public interface ITitleBar {
    View getView();
    void changeTitle(String string);
    void changeNum(String string);
}
